﻿using SaaSprin.Model.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaaSprin.Services
{
    public class UserController : ApiController
    {
        [Route("user/authenticate")]
        [HttpPost]
        public bool Authenticate(AuthenticationRequest request)
        {
            return Model.Company.Authenticate(request);
        }
        [Route("user/registration")]
        [HttpPost]
        public bool Registration(CompanyCreateRequest request)
        {
            return Model.Company.Create(request);
        }
        // GET api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET <controller>/5
        [HttpGet]
        public Model.User Get(int id)
        {
            return Model.User.Get(id);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public bool Delete(int id)
        {
            return Model.User.Delete(id);
        }
    }
}