﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using SaaSprin.Interfaces;
using SaaSprin.Model;
using SaaSprin.Model.Requests;
using SaaSprin.ParseStrategy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace SaaSprin.Services
{
    public class CompanyController : ApiController
    {
        [Route("company/update/{id:int}")]
        [HttpPost]
        public bool Update(int id, Company request)
        {
            Company updateRequest = new Company();//i have to create the the object the method Update as it is not static
            //  TODO: update on request company
            return updateRequest.Update(id, request);    
        }

        [Route("company/delete/{id:int}")]
        [HttpDelete]
        public bool Delete(int id)
        {            
            return Company.Delete(id);
        }

        [Route("company/create")]
        [HttpPut]
        public bool Create(CompanyCreateRequest request)
        {
            return Company.Create(request);
        }

        
    }
}