﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using SaaSprin.Interfaces;
using SaaSprin.Model;
using SaaSprin.Model.Filters;
using SaaSprin.ParseStrategy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace SaaSprin.Controllers
{
    public class InvoiceController : ApiController
    {
        [Route("invoice/upload/{provider}/{teamId:int}/{companyId:int}")]
        [HttpPost]
        public Invoice Upload(string provider, int companyId, int teamId)
        {
            var httpPostedFile = HttpContext.Current.Request.Files["Invoice"];
            string fileSavePath = null;
            string pdfText = null;
            if (httpPostedFile != null)
            {
                fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), httpPostedFile.FileName);
                httpPostedFile.SaveAs(fileSavePath);
            }

            using (PdfReader reader = new PdfReader(fileSavePath))
            {
                StringBuilder text = new StringBuilder();

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
                pdfText = text.ToString();
            }
            Console.WriteLine(pdfText);
            IParseStrategy strategy = ParseSrategyLocator.GetStrategy(provider);
            ParseResult result = strategy.Parse(pdfText);

            Invoice invoice = new Invoice();
            invoice.Amount = result.Amount;
            invoice.CompanyId = companyId;
            invoice.TeamId = teamId;
            invoice.GeneratedOn = result.Date;
            invoice.IsPaid = false;
            invoice.Provider = provider;
            return invoice.Create();
        }

        [Route("invoice/{companyId:int}/query")]
        [HttpPost]
        public List<Invoice> Find(int companyId, Filters filters)
        {
            return Invoice.Filter(companyId, filters.amountFilters, filters.dateFilters, filters.providerFilters, filters.teamFilters);
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}