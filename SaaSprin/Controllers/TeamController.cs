﻿using SaaSprin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaaSprin.Controllers
{
    public class TeamController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        [Route("team/create")]
        [HttpPut]
        public Team Create(Team team)
        {
            return team.Create();
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
             Team.Delete(id);
        }
    }
}