﻿using SaaSprin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaSprin.Interfaces
{
    public interface IParseStrategy
    {
        ParseResult Parse(string pdfText);
    }
}