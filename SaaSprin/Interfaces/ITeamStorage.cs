﻿using SaaSprin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaaSprin.Interfaces
{
    public interface ITeamStorage
    {
        Team Save(Team team);

        bool Delete(int teamId);

        Team Update(int teamId, Team team);

        Team GetById(int teamId);

        List<Team> GetByCompany(int companyId);
    }
}
