﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SaaSprin.Interfaces
{
    [DataContract]
    public abstract class Filter
    {
        public abstract String GetStringRepresentation();
    }
}
