﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaaSprin.Interfaces
{
    public interface IUserStorage
    {
        Model.User GetById(int id);

        bool DeleteById(int id);

        Model.User GetByUsernamePassword(string p1, string p2);

        Model.User createNewUser(string firstName, string lastName, string userName, string password,int companyId);
        
    }
}
