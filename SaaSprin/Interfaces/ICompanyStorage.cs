﻿using SaaSprin.Model.Requests;
using SaaSprin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaSprin.Interfaces
{
    public interface ICompanyStorage
    {
        Company GetByName(string companyName);        

        Company GetById(int id);
    
        Company Create(string name, string address, int numEmployees);

        bool DeleteById(int id);

        int GetLastId();

        Company UpdateDetails(int id, Company request);
    }
}