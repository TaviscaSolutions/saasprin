﻿
using SaaSprin.Model;
using SaaSprin.Model.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaSprin.Interfaces
{
    public interface IInvoiceStorage
    {
        Invoice Create(Invoice invoice);
        List<Invoice> Filter(int companyId, List<AmountFilter> amountFilters, List<DateFilter> dateFilters, List<ProviderFilter> providerFilters, List<TeamFilter> teamFilters);
    }
}