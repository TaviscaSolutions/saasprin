﻿using MySql.Data.MySqlClient;
using SaaSprin.Interfaces;
using SaaSprin.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace SaaSprin.MySqlImpl
{
    public class MySqlUserStorage : IUserStorage
    {

        String myConnectionString = ConfigurationManager.ConnectionStrings["mysqlConnectionString"].ConnectionString;

        public Model.User GetById(int id)
        {

            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call spGetUserById({0})", id), conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Model.User()
                        {
                            Id = dataReader.GetInt32("id"),
                            Username = dataReader.GetString("username"),
                            Password = dataReader.GetString("password"),
                            FirstName = dataReader.GetString("firstname"),
                            LastName = dataReader.GetString("lastname")
                        };
                    }

                    dataReader.Close();

                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }

            return null;
        }


        public Model.User GetByUsernamePassword(string username, string password)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_getuserbyusernamepassword('{0}', '{1}')", username, password), conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Model.User()
                        {
                            Id = dataReader.GetInt32("id"),
                            Username = dataReader.GetString("username"),
                            Password = dataReader.GetString("password"),
                            FirstName = dataReader.GetString("firstname"),
                            LastName = dataReader.GetString("lastname"),
                            CompanyId = dataReader.GetInt32("companyid")
                        };
                    }

                    dataReader.Close();

                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }

            return null;
        }

        public User createNewUser(string firstName, string lastName, string userName, string password, int companyId)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_insertUser('{0}', '{1}','{2}','{3}',{4})",
                        firstName,lastName,userName, password,companyId), conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Model.User()
                        {
                            Id = dataReader.GetInt32("id"),
                            Username = dataReader.GetString("username"),
                            Password = dataReader.GetString("password"),
                            FirstName = dataReader.GetString("firstname"),
                            LastName = dataReader.GetString("lastname"),
                            CompanyId = dataReader.GetInt32("companyid")
                        };
                    }

                    dataReader.Close();

                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }

            return null;
        }

        public bool DeleteById(int id)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_deleteUserById({0})",id), conn);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    if (rowsAffected != null)
                    {
                        return true;
                    }

                    conn.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return false;
        }
    }
}