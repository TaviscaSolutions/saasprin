﻿using MySql.Data.MySqlClient;
using SaaSprin.Interfaces;
using SaaSprin.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Collections.Generic;
namespace SaaSprin.MySqlImpl
{
    public class MySqlTeamStorage : ITeamStorage
    {
        String myConnectionString = ConfigurationManager.ConnectionStrings["mysqlConnectionString"].ConnectionString;

        public Model.Team Save(Model.Team team)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_insertTeam('{0}',{1},'{2}')",
                       team.Name, team.CompanyId, team.TeamLeader), conn);

                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Team()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            TeamLeader = dataReader.GetString("teamleader"),
                            CompanyId = dataReader.GetInt32("companyid")
                        };
                    }

                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return null;
        }

        public bool Delete(int teamId)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_deleteTeamById({0})",teamId), conn);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    if (rowsAffected != null)
                    {
                        return true;
                    }

                    conn.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return false;
        }

        public Model.Team Update(int teamId, Model.Team team)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_updateTeamdetails({0},'{1}','{2}',{3})",
                       teamId, team.Name, team.CompanyId, team.TeamLeader), conn);

                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Team()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            CompanyId = dataReader.GetInt32("companyid"),
                            TeamLeader = dataReader.GetString("teamleader")
                        };
                    }

                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return null;
        }

        public Model.Team GetById(int teamId)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_getteambyid({0})", teamId), conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Team()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            TeamLeader = dataReader.GetString("teamleader"),
                            CompanyId = dataReader.GetInt32("companyid")
                        };
                    }

                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return null;
        }

        public List<Model.Team> GetByCompany(int companyId)
        {
            try 
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                List<Team> list = new List<Team>();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_getTeamsByCompanyId({0})", companyId), conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        new Team()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            TeamLeader = dataReader.GetString("teamleader"),
                            CompanyId = dataReader.GetInt32("companyid")
                        };
                        list.Add(Team());
                    }
                    dataReader.Close();
                    return list;
                }
            }
            catch(Exception ex)
            {

            }
            return null;
        }

        private Team Team()
        {
            throw new NotImplementedException();
        }

        public string id { get; set; }

        public string compnayId { get; set; }
    }
}