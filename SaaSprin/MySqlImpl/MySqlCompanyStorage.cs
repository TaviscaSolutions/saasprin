﻿using MySql.Data.MySqlClient;
using SaaSprin.Interfaces;
using SaaSprin.Model;
using SaaSprin.Model.Requests;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SaaSprin.MySqlImpl
{
    public class MySqlCompanyStorage : ICompanyStorage
    {

        String myConnectionString = ConfigurationManager.ConnectionStrings["mysqlConnectionString"].ConnectionString;
        
        public Model.Company GetByName(string companyName)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_getcompanybyname('{0}')", companyName), conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Company()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            Address = dataReader.GetString("address"),
                            NumberOfEmployees = dataReader.GetInt32("employeecount")
                        };
                    }
                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }

            return null;
        }

        public Company GetById(int id)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_getcompanybyid({0})", id), conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Company()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            Address = dataReader.GetString("address"),
                            NumberOfEmployees = dataReader.GetInt32("employeecount")
                        };
                    }

                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw ;
            }
            return null;
        }

        public Company UpdateDetails(int id,Company request)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                var emp = request.NumberOfEmployees;
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_updateCompanyDetails({0},'{1}','{2}',{3})",
                       id,request.Name,request.Address,emp), conn);

                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Company()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            Address = dataReader.GetString("address"),
                            NumberOfEmployees = dataReader.GetInt32("employeecount")
                        };
                    }

                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return null;
        }

        public Company Create(String companyName,String address,Int32 numEmployees)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();                
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_insertCompany('{0}','{1}',{2})",
                       companyName , address,numEmployees ), conn);

                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Company()
                        {
                            Id = dataReader.GetInt32("id"),
                            Name = dataReader.GetString("name"),
                            Address = dataReader.GetString("address"),
                            NumberOfEmployees = dataReader.GetInt32("employeecount")
                        };
                    }

                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return null;
        }

        public bool DeleteById(int id)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_deleteCompanyById({0})",id), conn);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    if (rowsAffected != null)
                    {
                        return true;
                    }

                    conn.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return false;
        }

        public int GetLastId()
        {
            int id = 0;
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();                
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_getLastId({0})",id), conn);

                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    id = dataReader.GetInt32("id");
	                
                    dataReader.Close();
                    conn.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return id;
        }
    }
}