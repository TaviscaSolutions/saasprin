﻿using MySql.Data.MySqlClient;
using SaaSprin.Interfaces;
using SaaSprin.Model;
using SaaSprin.Model.Filters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SaaSprin.MySqlImpl
{
    public class MySqlInvoiceStorage : IInvoiceStorage
    {
        String myConnectionString = ConfigurationManager.ConnectionStrings["mysqlConnectionString"].ConnectionString;

        public Model.Invoice Create(Model.Invoice invoice)
        {
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    MySqlCommand cmd = new MySqlCommand(String.Format("call sp_insertInvoice({0}, {1}, '{2}', {3}, '{4}', {5})",
                       invoice.TeamId, invoice.CompanyId, invoice.Provider, invoice.Amount, invoice.GeneratedOn.ToString("yyyy-MM-dd"), invoice.IsPaid ? "b'1'" : "b'0'"), conn);

                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        return new Invoice()
                        {
                            Id = dataReader.GetInt32("id"),
                            TeamId = dataReader.GetInt32("teamid"),
                            CompanyId = dataReader.GetInt32("companyid"),
                            Amount = dataReader.GetDecimal("amount"),
                            GeneratedOn = dataReader.GetDateTime("generatedon"),
                            IsPaid = dataReader.GetBoolean("ispaid"),
                            Provider = dataReader.GetString("provider")
                        };
                    }

                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }
            return null;
        }


        public List<Invoice> Filter(int companyId, List<AmountFilter> amountFilters, List<DateFilter> dateFilters, List<ProviderFilter> providerFilters, List<TeamFilter> teamFilters)
        {
            List<Invoice> retrievedInvoices = new List<Invoice>();
            try
            {
                var conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
                using (conn)
                {
                    string amountStrFilter = null;
                    if (amountFilters.Count > 0)
                    {
                        amountStrFilter = string.Join(" or ", ProcessFilters(amountFilters));
                        amountStrFilter = string.Format("( {0} )", amountStrFilter);
                    }

                    string dateStrFilter = null;
                    if (dateFilters.Count > 0)
                    {
                        dateStrFilter = string.Join(" or ", ProcessFilters(dateFilters));
                        dateStrFilter = string.Format("( {0} )", dateStrFilter);
                    }

                    string providerStrFilter = null;
                    if (providerFilters.Count > 0)
                    {
                        providerStrFilter = string.Join(" or ", ProcessFilters(providerFilters));
                        providerStrFilter = string.Format("( {0} )", providerStrFilter);
                    }

                    string teamStrFilter = null;
                    if (teamFilters.Count > 0)
                    {
                        teamStrFilter = string.Join(" or ", ProcessFilters(teamFilters));
                        teamStrFilter = string.Format("( {0} )", teamStrFilter);
                    }

                    List<string> filterTypes = new List<string>();
                    if (string.IsNullOrEmpty(amountStrFilter) == false)
                        filterTypes.Add(amountStrFilter);

                    if (string.IsNullOrEmpty(dateStrFilter) == false)
                        filterTypes.Add(dateStrFilter);

                    if (string.IsNullOrEmpty(providerStrFilter) == false)
                        filterTypes.Add(providerStrFilter);

                    if (string.IsNullOrEmpty(teamStrFilter) == false)
                        filterTypes.Add(teamStrFilter);

                    string query = string.Format("select * from invoice where companyid = {0} and {1};", companyId, string.Join(" and ", filterTypes));

                    MySqlCommand cmd = new MySqlCommand(query, conn);
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        retrievedInvoices.Add(new Invoice()
                        {
                            Id = dataReader.GetInt32("id"),
                            TeamId = dataReader.GetInt32("teamid"),
                            CompanyId = dataReader.GetInt32("companyid"),
                            Amount = dataReader.GetDecimal("amount"),
                            GeneratedOn = dataReader.GetDateTime("generatedon"),
                            IsPaid = dataReader.GetBoolean("ispaid"),
                            Provider = dataReader.GetString("provider")
                        });
                    }
                    dataReader.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw;
            }

            return retrievedInvoices;
        }

        private static List<String> ProcessFilters(List<TeamFilter> filters)
        {
            var strFilters = new List<string>();
            foreach (var filter in filters)
            {
                strFilters.Add(filter.GetStringRepresentation());
            }
            return strFilters;
        }

        private static List<String> ProcessFilters(List<AmountFilter> filters)
        {
            var strFilters = new List<string>();
            foreach (var filter in filters)
            {
                strFilters.Add(filter.GetStringRepresentation());
            }
            return strFilters;
        }

        private static List<String> ProcessFilters(List<DateFilter> filters)
        {
            var strFilters = new List<string>();
            foreach (var filter in filters)
            {
                strFilters.Add(filter.GetStringRepresentation());
            }
            return strFilters;
        }

        private static List<String> ProcessFilters(List<ProviderFilter> filters)
        {
            var strFilters = new List<string>();
            foreach (var filter in filters)
            {
                strFilters.Add(filter.GetStringRepresentation());
            }
            return strFilters;
        }
    }
}