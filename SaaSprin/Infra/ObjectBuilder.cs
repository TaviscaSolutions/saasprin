﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaSprin.Infra
{
    public class ObjectBuilder
    {
        private static Dictionary<Type, Object> _registrations;

        static ObjectBuilder()//this is the constructor
        {
            _registrations = new Dictionary<Type, object>();
            _registrations.Add(typeof(ICompanyStorage), new MySqlImpl.MySqlCompanyStorage());
            _registrations.Add(typeof(IUserStorage), new MySqlImpl.MySqlUserStorage());
            _registrations.Add(typeof(ITeamStorage), new MySqlImpl.MySqlTeamStorage());
            _registrations.Add(typeof(IInvoiceStorage), new MySqlImpl.MySqlInvoiceStorage());
        }


        public static T Build<T>()
        {
            return (T)_registrations[typeof(T)];//now the constructor is called
        }
    }
}