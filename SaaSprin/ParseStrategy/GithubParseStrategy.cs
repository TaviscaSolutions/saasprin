﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using SaaSprin.Model;
using System.Globalization;

namespace SaaSprin.ParseStrategy
{
    public class GithubParseStrategy : IParseStrategy 
    {
        public Model.ParseResult Parse(string pdfText)
        {
            var result = new ParseResult();
            result.Amount = decimal.Parse(RegexUtils.ShowMatch(pdfText, @"\b[$]*\d{1,3}?\.\d{1,3}?\b"));
            result.Date = DateTime.Parse(RegexUtils.ShowMatch(pdfText, @"\b[0-9][0-9][/][0-9][0-9][/][0-9][0-9]\b"));
            return result;
        }
    }
}