﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaSprin.ParseStrategy
{
    public static class ParseSrategyLocator
    {
        private static Dictionary<string, IParseStrategy> _registrations;

        static ParseSrategyLocator()
        {
            _registrations = new Dictionary<string, IParseStrategy>();
            _registrations.Add("github", new GithubParseStrategy());
            _registrations.Add("appacitive", new AppacitiveParseStrategy());
        }

        public static IParseStrategy GetStrategy(string provider)
        {

            var strategy = _registrations[provider];
            if (strategy == null)
                throw new Exception("Missing type.");
            return strategy;
        }


    }
}