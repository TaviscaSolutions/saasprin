﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SaaSprin.ParseStrategy
{
    public class RegexUtils
    {
        public static string ShowMatch(string pdfText, string pattern)
        {
            string[] buffer = new string[200];

            MatchCollection mc = Regex.Matches(pdfText, pattern);
            foreach (Match m in mc)
            {
                return (m.Value);
            }
            return null;
        }
    }
}