﻿$(document).ready(function () {
    $("#submit").click(function (e) {
        var name = $('#txtCompanyName').val();
        var address = $('#txtAddress').val();
        var noOfEmployees = $('#txtdrpdwnNOE').val();
        var firstName = $('#txtFirstName').val() ;
        var lastName = $('#txtLastName').val();
        var userName = $('#txtUsername').val();
        var password = $("#txtPassword").val();
        var cpassword = $("#txtCpassword").val();
        if ($("#txtName").val() == "")
        {
            $("#txtName").focus();
            $("#errorBox").html("*Enter The Company Name");
            return false;
        }
        else if($('#txtAddress').val()=="") 
        {
            $("#txtAddress").focus();
            $("#errorBox").html("*Enter The Company Address");
            return false;
        }
        else if($('#txtdrpdwnNOE').val()=="")
        {
            $("#txtdrpdwnNOE").focus();
            $("#errorBox").html("*Enter The Number Of Employees");
            return false;
        }
        else if ($('#txtFirstName').val()=="")
        {
            $("#txtFirstName").focus();
            $("#errorBox").html("*Enter The First Name");
            return false;
        }
        else if($('#txtLastName').val()=="")
        {
            $("#txtLastName").focus();
            $("#errorBox").html("*Enter The Last Name");
            return false;
        }
        else if ($('#txtUsername').val()=="")
        {
            $("#txtUername'").focus();
            $("#errorBox").html("*Enter The UserName");
            return false;
        }
        else if($("#txtPassword").val()=="")
        {
            $("#txtPassword").focus();
            $("#errorBox").html("*Enter The Password");
            return false;
        }
        else if(password != cpassword)
        {
            $("#txtCpassword").focus();
            $("#errorBox").html("*Password Not Mached");
            return false;
        }
        else if ($(name != '' && address != '' && firstName != '' && lastName != '' && userName != '' && password != '' && cpassword != '' ))
        {
            $("#errorBox").html("Form Submitted Successfully");
            e.preventDefault();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "http://localhost:18241/user/registration",
                dataType: "json",
                data: "{'Name':'" + $('#txtName').val() + "','Address':'" + $('#txtAddress').val() + "','NumberOfEmployees':'" + $('#txtdrpdwnNOE').val() + "','FirstName':'" + $('#txtFirstName').val() + "','LastName':'" + $('#txtLastName').val() + "','Username':'" + $('#txtUsername').val() + "','Password':'" + $('#txtPassword').val() + "'}",
                success: function (data) {
                    if (data === true) {
                        window.location.href = 'http://localhost:18241/UITheme/Index.html';
                        $('#txtName').val('');
                        $('#txtAddress').val('');
                        $('#txtdrpdwnNOE').val('');
                        $('#txtFirstName').val('');
                        $('#txtLastName').val('');
                        $('#txtUsername').val('');
                        $('#txtPassword').val('');
                    }
                    else
                    {
                        $("#errorBox").html("    *Entered Company Already Exists");
                        return false;
                        window.location.href = 'http://localhost:18241/UITheme/pages/examples/register.html';
                    }
                },
                error: function (data) {
                    window.location.href = 'http://localhost:18241/UITheme/pages/examples/register.html';
                }

            });
        }
    });
});