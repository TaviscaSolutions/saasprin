﻿$(document).ready(function () {
    $("#submit").click(function (e) {
        var name = $('#txtCompanyName').val();
        var username = $('#txtUsername').val();
        var password = $('#txtPassword').val();
        if ($("#txtCompanyName").val() == "")
        {
            $("#txtCompanyName").focus();
            $("#errorBox").html("*Enter The Company Name");
            return false;
        }
        else if ($('#txtUsername').val() == "") {
            $("#txtUsername").focus();
            $("#errorBox").html("*Enter The Username");
            return false;
        }
        else if ($('#txtPassword').val() == "") {
            $("#txtPassword").focus();
            $("#errorBox").html("*Enter The Password");
            return false;
        }
        else if($(name !='' && username!='' && password!=''))
        {
            e.preventDefault();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "http://localhost:18241/user/authenticate",
                dataType: "json",
                data: "{'CompanyName':'"+ $('#txtCompanyName').val() +"','Username':'"+ $('#txtUsername').val() +"','Password':'"+ $('#txtPassword').val() +"'}",
                success: function (data) {
                    if (data === true) {
                        window.location.href = 'http://localhost:18241/UITheme/Index.html';
                        $('#txtCompanyName').val('');
                        $('#txtUsername').val('');
                        $('#txtPassword').val('');
                    }
                    else {
                        $("#errorBox").html("*Sorry USERNAME or PASSWORD is incorrect");
                        return false;
                        window.location.href = 'http://localhost:18241/UITheme/pages/examples/login.html';
                    }
                },
                error: function (data) {
                    window.location.href = 'http://localhost:18241/UITheme/pages/examples/login.html';
                }

            });
        }

    });
});