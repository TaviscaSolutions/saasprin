﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model.Requests
{
    [Serializable]
    [DataContract]
    public class CompanyCreateRequest
    {
        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public int NumberOfEmployees { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String UserName { get; set; }

        [DataMember]
        public String Password { get; set; }
    }
}