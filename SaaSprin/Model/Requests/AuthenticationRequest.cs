﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SaaSprin.Model.Requests
{
    [Serializable]
    [DataContract]
    public class AuthenticationRequest
    {
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}