﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model.Filters
{
    [DataContract]
    [KnownType(typeof(Filter))]
    public class IsPaidFilter : Filter
    {
        [DataMember]
        public bool IsPaid { get; set; }
        public override string GetStringRepresentation()
        {
            return string.Format(" ispaid = {0} ", this.IsPaid ? "1" : "0");
        }
    }
}