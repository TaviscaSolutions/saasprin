﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model.Filters
{
    [DataContract]
    public class Filters
    {

        public Filters()
        {
            amountFilters = new List<AmountFilter>();
            dateFilters = new List<DateFilter>();
            providerFilters = new List<ProviderFilter>();
            teamFilters = new List<TeamFilter>();
        }
        [DataMember]
        public List<AmountFilter> amountFilters { get; set; }

        [DataMember]
        public List<DateFilter> dateFilters { get; set; }

        [DataMember]
        public List<ProviderFilter> providerFilters { get; set; }

        [DataMember]
        public List<TeamFilter> teamFilters { get; set; }
    }
}