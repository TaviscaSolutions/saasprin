﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model.Filters
{
    [DataContract]
    [KnownType(typeof(Filter))]
    public class TeamFilter : Filter
    {
        [DataMember]
        public List<int> TeamIds { get; set; }

        public override string GetStringRepresentation()
        {
            return string.Format("teamid in ( {0} )", string.Join(" , ", this.TeamIds));
        }
    }
}