﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model.Filters
{
    [DataContract]
    [KnownType(typeof(Filter))]
    public class ProviderFilter :Filter
    {
        [DataMember]
        public String Provider { get; set; }

        public override string GetStringRepresentation()
        {
            return string.Format("provider = '{0}' ", this.Provider);
        }
    }
}