﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model.Filters
{
    [DataContract]
    [KnownType(typeof(Filter))]
    public class AmountFilter : Filter
    {
        [DataMember]
        public decimal MinValue { get; set; }

        [DataMember]
        public decimal MaxValue { get; set; }

        public override string GetStringRepresentation()
        {
            return string.Format("amount between {0} and {1}", this.MinValue, this.MaxValue);
        }
    }
}