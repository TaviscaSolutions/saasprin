﻿using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model.Filters
{
    [DataContract]
    [KnownType(typeof(Filter))]
    public class DateFilter : Filter
    {
        [DataMember]
        public DateTime MinValue { get; set; }

        [DataMember]
        public DateTime MaxValue { get; set; }

        public override string GetStringRepresentation()
        {
            return string.Format("generatedon between '{0}' and '{1}'", this.MinValue, this.MaxValue);
        }
    }
}