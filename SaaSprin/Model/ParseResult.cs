﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaSprin.Model
{
    public class ParseResult
    {
        public DateTime Date { get; set; }

        public decimal Amount { get; set; }
    }
}