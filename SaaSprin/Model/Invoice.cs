﻿using SaaSprin.Infra;
using SaaSprin.Interfaces;
using SaaSprin.Model.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaaSprin.Model
{
    public class Invoice
    {
        public int Id { get; set; }

        public DateTime GeneratedOn { get; set; }

        public decimal Amount { get; set; }

        public int TeamId { get; set; }

        public string Provider { get; set; }

        public int CompanyId { get; set; }

        public bool IsPaid { get; set; }

        public Invoice Create()
        {
            var invoiceStorage = ObjectBuilder.Build<IInvoiceStorage>();
            return invoiceStorage.Create(this);        
        }

        public static List<Invoice> Filter(int companyId, List<AmountFilter> amountFilters, List<DateFilter> dateFilters, List<ProviderFilter> providerFilters, List<TeamFilter> teamFilters)
        {
            var invoiceStorage = ObjectBuilder.Build<IInvoiceStorage>();
            return invoiceStorage.Filter(companyId, amountFilters, dateFilters, providerFilters, teamFilters);
        }
    }
}