﻿using SaaSprin.Infra;
using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using SaaSprin.Model.Requests;

namespace SaaSprin.Model
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id   {get;set;}

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String Username { get; set; }

        [DataMember]
        public String Password { get; set; }

        [DataMember]
        public int CompanyId { get; set; }

        public void Save()
        {
            
        }

        public static User Get(int id)
        {
            var userStorage = ObjectBuilder.Build<IUserStorage>();
            return userStorage.GetById(id);
        }
       
        public static bool Delete(int id)
        {
            var UserSorageImplementaion = ObjectBuilder.Build<IUserStorage>();
            User deleteRequest = UserSorageImplementaion.GetById(id);
            if (id != null)
            {
                //if the User exists delete the user.
                bool deleteUser = UserSorageImplementaion.DeleteById(id);
                if (deleteUser == true)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}