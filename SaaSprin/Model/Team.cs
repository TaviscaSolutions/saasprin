﻿using SaaSprin.Infra;
using SaaSprin.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model
{
    [DataContract]
    public class Team
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String TeamLeader { get; set; }

        [DataMember]
        public int CompanyId { get; set; }

        public Team Create()
        {
            var teamStorage = ObjectBuilder.Build<ITeamStorage>();
            return teamStorage.Save(this);
        }


        public static bool Delete(int id)
        {
            var TeamSorageImplementaion = ObjectBuilder.Build<ITeamStorage>();
            Team deleteRequest = TeamSorageImplementaion.GetById(id);
            if (id != null)
            {
                //if the User exists delete the user.
                bool deleteUser = TeamSorageImplementaion.Delete(id);
                if (deleteUser == true)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}