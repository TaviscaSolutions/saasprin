﻿using SaaSprin.Infra;
using SaaSprin.Interfaces;
using SaaSprin.Model.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SaaSprin.Model
{
    [DataContract]
    public class Company
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String Address { get; set; }

        [DataMember]
        public int NumberOfEmployees { get; set; }

        [DataMember]
        public String Logo { get; set; }

        public static bool Authenticate(AuthenticationRequest request)
        {
            var userStorage = ObjectBuilder.Build<IUserStorage>();
            User user = userStorage.GetByUsernamePassword(request.Username, request.Password);

            if (user == null)
                return false;

            var comapanyStorage = ObjectBuilder.Build<ICompanyStorage>();
            Company company = comapanyStorage.GetByName(request.CompanyName);

            if (company != null)                
                if (user.CompanyId == company.Id)
                    return true;

            return false;
        }
        
        public bool Update(int id,Company request)        
        {
            
            var companyOldDetails = ObjectBuilder.Build<ICompanyStorage>();
            Company oldDetails = companyOldDetails.GetById(id);

            //now i have all the details of the company in old_details object.
            if (oldDetails != null)
            {
                //now that the company is found,i have to update the details of the company
                var companyNewDetails = ObjectBuilder.Build<ICompanyStorage>();//   ASK:  if this is required or i can use the earlier one
                Company newDetails = companyNewDetails.UpdateDetails(id,request);

                if (newDetails!=null)
                {
                    return true;//update succesfull
                }
                else
                    return false;//update failed

            }
            else
                return false;//company id not found          
        }

        public static bool Create(CompanyCreateRequest request)
        {
            var companyCreate = ObjectBuilder.Build<ICompanyStorage>();
            Company companyPresent = companyCreate.GetByName(request.Name);

            //if the company does not exist
            //create new company;
            if (companyPresent==null)
            {
                //first create the company and then create the user for that company
                Company newCompany = companyCreate.Create(request.Name,request.Address,request.NumberOfEmployees);
                if (newCompany != null)
                {
                    //create user NOW
                    //a user has company as well,so get the company id of the last company FOR NOW,
                    //and add 1 to it and update the companyid as well with it
                    
                    //getting the last company id

                    // TODO Fire get last id in create company sp

                    int companyId = newCompany.Id;
                    //int companyId = company_create.GetLastId();

                    //now creating the user.
                    var createUserForCompany = ObjectBuilder.Build<IUserStorage>();
                    User new_user = createUserForCompany.createNewUser(request.FirstName,request.LastName,request.UserName,request.Password,companyId);
                    if (new_user!=null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                Console.WriteLine("Company Already exists!!");
                return false;
            }
        }

        public static bool Delete(int id)
        {
            var companySorageImplementaion = ObjectBuilder.Build<ICompanyStorage>();
            Company deleteRequest = companySorageImplementaion.GetById(id);
            if (id != null)
            {
                //if the company exists delete the company
                bool deleteCompany = companySorageImplementaion.DeleteById(id);
                if (deleteCompany==true)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;

        }

        



        public string CompanyId { get; set; }

       
    }
}