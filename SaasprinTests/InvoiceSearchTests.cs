﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SaaSprin.Infra;
using SaaSprin.Interfaces;
using SaaSprin.Model;
using System.Collections.Generic;
using SaaSprin.Model.Filters;
using SaaSprin.MySqlImpl;

namespace SaasprinTests
{
    [TestClass]
    public class InvoiceSearchTests
    {
        [TestMethod]
        public void SearchInvoiceTest()
        {
            var invoiceStorage = new MySqlInvoiceStorage();
            List<Invoice> invoices = new List<Invoice>();
            for (int i = 0; i < 10; i++)
            {
                var invoice = new Invoice()
                {
                    Amount = i * 10,
                    CompanyId = i,
                    GeneratedOn = DateTime.Now,
                    IsPaid = true,
                    Provider = "github",
                    TeamId = i

                };
                invoice.Create();
                invoices.Add(invoice);
            }

            var filter1 = new TeamFilter();
            filter1.TeamIds = new List<int>(){5};
            //var returnedInvoices = invoiceStorage.Filter(5, new List<Filter>() { filter1 });
            //Assert.AreEqual(returnedInvoices.Count, 1);

        }
    }
}
